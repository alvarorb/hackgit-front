import { LitElement, html} from 'lit-element';
import '../hg-header/hg-header.js';
import '../hg-sidebar/hg-sidebar.js';
import '../hg-main-file/hg-main-file.js';
import '../hg-main-admin-profile/hg-main-admin-profile.js';
import '../hg-main-admin-repo/hg-main-admin-repo.js';
import '../hg-footer/hg-footer.js';

class HgApp extends LitElement {

    constructor(){
        super();

    }

    render(){
        return html`
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z"  crossorigin="anonymous" />
            <hg-header></hg-header>
            <div class="row">
                <hg-sidebar 
                    @view-admin-repo-event="${this.viewAdminRepo}" 
                    @view-admin-profile-event="${this.viewAdminProfile}" 
                    @view-file-event="${this.viewFile}" 
                    class="col-2">
                </hg-sidebar>
                <hg-main-file @generate-file-event="${this.generateFile}" class="col-10 d-none"></hg-main-file>
                <hg-main-admin-profile class="col-10 d-none"></hg-main-admin-profile>
                <hg-main-admin-repo class="col-10 d-none"></hg-main-admin-repo>
            </div>
            <hg-footer></hg-footer>
        `;
    }

    viewAdminRepo(e){
        console.log("viewAdminRepo");

        this.shadowRoot.querySelector("hg-main-file").classList.add("d-none");
        this.shadowRoot.querySelector("hg-main-admin-profile").classList.add("d-none");

        this.shadowRoot.querySelector("hg-main-admin-repo").classList.remove("d-none");
    }

    viewAdminProfile(e){
        console.log("viewAdminProfile");

        this.shadowRoot.querySelector("hg-main-file").classList.add("d-none");
        this.shadowRoot.querySelector("hg-main-admin-repo").classList.add("d-none");

        this.shadowRoot.querySelector("hg-main-admin-profile").classList.remove("d-none");

    }

    viewFile(e){
        console.log("viewFile");

        this.shadowRoot.querySelector("hg-main-admin-profile").classList.add("d-none");
        this.shadowRoot.querySelector("hg-main-admin-repo").classList.add("d-none");

        this.shadowRoot.querySelector("hg-main-file").classList.remove("d-none");
    }

    generateFile(e){
        console.log("generateFile");
    }
}

customElements.define('hg-app', HgApp);