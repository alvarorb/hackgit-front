import { LitElement, html} from 'lit-element';

class HgSidebar extends LitElement {
    render(){
        return html`
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z"  crossorigin="anonymous" />
            <div class="wrapper">
                <!-- Sidebar -->
                <nav id="sidebar">
                    <div class="sidebar-header">
                        <h3>Bootstrap Sidebar</h3>
                    </div>
            
                    <ul class="list-unstyled components">
                        <li>
                            <a href="#" @click="${this.viewAdminRepo}">Admin Repos</a>
                        </li>
                        <li>
                            <a href="#" @click="${this.viewAdminProfile}">Admin Profiles</a>
                        </li>
                        <li>
                            <a href="#" @click="${this.viewFile}">Generate File</a>
                        </li>
                    </ul>
                </nav>
            </div>
        `;
    }

    viewAdminRepo(){
        console.log("viewAdminRepo");
    
        this.dispatchEvent(new CustomEvent("view-admin-repo-event",{})); 
    }

    viewAdminProfile(){
        console.log("viewAdminProfile");
    
        this.dispatchEvent(new CustomEvent("view-admin-profile-event",{})); 
    }

    viewFile(){
        console.log("viewFile");
    
        this.dispatchEvent(new CustomEvent("view-file-event",{})); 
    }
}

customElements.define('hg-sidebar', HgSidebar);