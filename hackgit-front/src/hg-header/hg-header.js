import { LitElement, html} from 'lit-element';

class HgHeader extends LitElement {
    render(){
        return html`
            <h1>Listado de Repositorios Git y Usuarios</h1>
        `;
    }
}

customElements.define('hg-header', HgHeader);