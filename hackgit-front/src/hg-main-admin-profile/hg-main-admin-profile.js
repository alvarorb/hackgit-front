import { LitElement, html} from 'lit-element';
import '../hg-lista-perfiles/hg-lista-perfiles.js';

class HgMainAdminProfile extends LitElement {
    render(){
        return html`
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z"  crossorigin="anonymous" />
        <h2>Administración de perfiles</h2>
        <hg-lista-perfiles @selected-profile-event="${this.selectedProfile}" titulo="Lista perfiles"></hg-lista-perfiles>
        <row><a @click="${this.addProfile}" class="btn btn-success"><strong>Añadir perfil</strong></a></row>
        `;
    }

    addProfile(){
        console.log("Añadimos un perfil");
    }
}

customElements.define('hg-main-admin-profile', HgMainAdminProfile);