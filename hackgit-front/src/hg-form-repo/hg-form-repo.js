import { LitElement, html} from 'lit-element';

class HgFormRepo extends LitElement {

    static get properties(){
        return {
            repo: {type: Object},
            editingRepo: {type: Boolean}
        }
    }

    constructor(){
        super();
        console.log("Contructor form");
        this.initFormData();
    }

    render(){
        return html`
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z"  crossorigin="anonymous" />
            <div>
                <form>
                    <div class="form-group">
                        <label>Id repositorio</label>
                        <input type="text" @input="${this.updateId}" .value="${this.repo.id}" ?disabled="${this.editingRepo}" id="repoId" class="form-control" />
                    </div>
                    <div class="form-group">
                        <label>Nombre repositorio</label>
                        <input type="text" @input="${this.updateName}" .value="${this.repo.repoName}" ?disabled="${this.editingRepo}" id="repoName" class="form-control" />
                    </div>
                    <div class="form-group">
                        <label>Url de Git</label>
                        <input type="text" @input="${this.updateUrl}" .value="${this.repo.urlg}" ?disabled="${this.editingRepo}" id="repoUrl" class="form-control" />
                    </div>
                    <button @click="${this.goBack}" class="btn btn-primary"><strong>Reset</strong></button>
                    <button @click="${this.storeRepo}" class="btn btn-success"><strong>Guardar</strong></button>
                </form>
            </div>
        `;
    }

    updateId(e){
        console.log("updateId");
        console.log("Actualizando la propiedad id con el valor "+ e.target.value);
        this.repo.id=e.target.value;
    }

    updateName(e){
        console.log("updateName");
        console.log("Actualizando la propiedad name con el valor "+ e.target.value);
        this.repo.repoName=e.target.value;
    }

    updateUrl(e){
        console.log("updateUrl");
        console.log("Actualizando la propiedad url con el valor "+ e.target.value);
        this.repo.urlg=e.target.value;
    }

    goBack(e){
        console.log("goBack");
        e.preventDefault();

        //inicializamos el formulario
        this.initFormData();

        this.dispatchEvent(new CustomEvent("form-repo-reset-event",{}));
    }

    storeRepo(e){
        console.log("storeRepo");
        e.preventDefault();

        console.log(this.repo);

        let xhr = new XMLHttpRequest();

        xhr.onload = () => {
            console.log("Estado peticion: "+ xhr.status);
            if(xhr.status === 200){
                console.log("Se ha añadido una nueva repo");
            }else{
                console.log("Se ha añadido una nueva repo (MOCK)");
            }
            this.dispatchEvent(new CustomEvent("form-repo-store-event",{}));

        }

        xhr.open("POST", "http://localhost:8081/hackgit/v1/repos", true);
        xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        xhr.send(JSON.stringify(this.repo));

    }

    initFormData(){
        console.log("initFormData");

        this.repo = {};
        this.repo.id = "";
        this.repo.repoName = "";
        this.repo.urlg = "";

        this.editingRepo = false;
    }
}

customElements.define('hg-form-repo', HgFormRepo);
