import { LitElement, html} from 'lit-element';

class HgListaPerfiles extends LitElement {

    static get properties(){
        return {
            perfiles: {type:Array},
            titulo: {type:String}
        };
    }

    constructor(){
        super();

        this.perfiles = [];

        this.getPerfilesData();

    }

    render(){

        return html`
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z"  crossorigin="anonymous" />
        <h3>${this.titulo}</h3>
        <div class="table-responsive">
            <table class="table">
                <caption>Numero de repos: <span class="badge badge-pill badge-primary">${this.perfiles.length}</span></caption>
                <thead>
                <tr>
                    <th scope="col">Seleccionar</th>
                    <th scope="col">ID</th>
                    <th scope="col">Nombre</th>
                    <th scope="col">Correo</th>
                </tr>
                </thead>
                <tbody>
                ${this.perfiles.map(
                    perfil => html`
                    <tr>
                        <td><input @change="${this.selectedProfile}" class="form-check-input" type="radio" name="perfil" id="perfil" value="${perfil.id}"></td>
                        <td>${perfil.id}</td>
                        <td>${perfil.nombre}</td>
                        <td>${perfil.correo}</td>
                    </tr>
                    `
                )}
                </tbody>
            </table>
        </div>
        `;
    }

    getPerfilesData(){
        console.log("getPerfilesData");
        console.log("Obteniendo datos de los perfiles");

        let xhr = new XMLHttpRequest();

        xhr.onload = () => {
            console.log("Estado peticion: "+ xhr.status);
            if(xhr.status === 200){
                let apiResponse = JSON.parse(xhr.responseText);
                this.perfiles=apiResponse;
            }
        }

        xhr.open("GET", "http://localhost:8081/hackgit/v1/perfiles", true);
        xhr.send();

        if(xhr.status != 200){
             this.getPerfilesMock();
        }
    }

    getPerfilesMock(){
        console.log("getPerfilesMock");
        this.perfiles = [
            {
                id: "1",
                nombre: "Juan",
                correo: "juan@gmail.com"
            },
            {
                id: "2",
                nombre: "Antonio",
                correo: "antonio@gmail.com"
            }
        ];
    }

    selectedProfile(e){
        console.log(e.target.value);

        this.dispatchEvent(new CustomEvent("selected-profile-event",{
            detail: {
                selectedId: e.target.value
            }
        }));
    }
}

customElements.define('hg-lista-perfiles', HgListaPerfiles);
