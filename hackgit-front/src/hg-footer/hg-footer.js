import { LitElement, html} from 'lit-element';

class HgFooter extends LitElement {
    render(){
        return html`
            <h5>@Hackaton Git Front y Back - 2020</h5>
        `;
    }
}

customElements.define('hg-footer', HgFooter);