import { LitElement, html} from 'lit-element';

class HgListaRepos extends LitElement {

    static get properties(){
        return {
            repos: {type:Array},
            titulo: {type:String},
            refresh: {type:Boolean}
        };
    }

    constructor(){
        super();

        this.repos = [];
        this.getReposData();
        this.refresh=false;
    }

    render(){

        return html`
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z"  crossorigin="anonymous" />
        <h3>${this.titulo}</h3>
        <div class="table-responsive">
            <table class="table">
                <caption>Numero de repos: <span class="badge badge-pill badge-primary">${this.repos.length}</span></caption>
                <thead>
                <tr>
                    <th scope="col">Seleccionar</th>
                    <th scope="col">ID</th>
                    <th scope="col">RepoName</th>
                    <th scope="col">Url git</th>
                </tr>
                </thead>
                <tbody>
                ${this.repos.map(
                    repo => html`
                    <tr>
                        <td><input @change="${this.selectedRepo}" class="form-check-input" type="radio" name="repo" id="repo" value="${repo.id}"></td>
                        <td>${repo.id}</td>
                        <td>${repo.repoName}</td>
                        <td>${repo.urlg}</td>
                    </tr>
                    `
                )}
                </tbody>
            </table>
        </div>
        `;
    }

    updated(changedProperties){
        console.log("updated");

        if(changedProperties.has("refresh")){

            console.log("Actualizamos la lista");
            if (this.refresh === true){
                this.getReposData();
            }
        }
    }

    getReposData(){
        console.log("getReposData");
        console.log("Obteniendo datos de los repos");

        let xhr = new XMLHttpRequest();

        xhr.onload = () => {
            console.log("Estado peticion: "+ xhr.status);
            if(xhr.status === 200){
                let apiResponse = JSON.parse(xhr.responseText);
                this.repos=apiResponse;

            }
        }

        xhr.open("GET", "http://localhost:8081/hackgit/v1/repos", true);
        xhr.send();

        if(xhr.status != 200){
             this.getReposMock();
        }

        this.refresh=false;
    }

    getReposMock(){
        console.log("getReposMock");
        this.repos = [
            {
                id: "1",
                repoName: "repo1",
                urlg: "https://repo1/git"
            },
            {
                id: "2",
                repoName: "repo2",
                urlg: "https://repo1/git"
            }
        ];
    }

    selectedRepo(e){
        console.log(e.target.value);

        this.dispatchEvent(new CustomEvent("selected-repo-event",{
            detail: {
                selectedId: e.target.value
            }
        }));
    }
}

customElements.define('hg-lista-repos', HgListaRepos);
