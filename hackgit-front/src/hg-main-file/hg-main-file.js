import { LitElement, html} from 'lit-element';
import '../hg-lista-repos/hg-lista-repos.js';
import '../hg-lista-perfiles/hg-lista-perfiles.js';

class HgMainFile extends LitElement {

    static get properties(){
        return {
            idRepoOrigen: {type: String},
            idRepoDestino: {type: String},
            idPerfil: {type: String},
            tipoF: {type: String}
        }
    }

    constructor(){
        super();

        this.idRepoOrigen = 0;
        this.idRepoDestino = 0;
        this.idPerfil = 0;
        this.tipoF = "";
    }

    render(){
        return html`
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z"  crossorigin="anonymous" />
            <h2>Main File</h2>
            <hg-lista-repos @selected-repo-event="${this.selectedRepoOrigen}" titulo="Lista repos origen"></hg-lista-repos>
            <hg-lista-repos @selected-repo-event="${this.selectedRepoDestino}" titulo="Lista repos destino"></hg-lista-repos>
            <hg-lista-perfiles @selected-profile-event="${this.selectedProfile}" titulo="Lista perfiles"></hg-lista-perfiles>
            <row><a href="#" target="blank_" @click="${this.generarEnlaceFicheroSh}" class="btn btn-success"><strong>Generar fichero SH</strong></a>
            <a href="#" target="blank_" @click="${this.generarEnlaceFicheroBat}" class="btn btn-success"><strong>Generar fichero BATCH</strong></a></row>
        `;
    }

    selectedProfile(e){
        console.log("Perfil seleccionado "+e.detail.selectedId);

        this.idPerfil=e.detail.selectedId;
    }

    selectedRepoOrigen(e){
        console.log("Repo origen seleccionado "+e.detail.selectedId);

        this.idRepoOrigen=e.detail.selectedId;
    }

    selectedRepoDestino(e){
        console.log("Repo destino seleccionado "+e.detail.selectedId);

        this.idRepoDestino=e.detail.selectedId;
    }

    generarEnlaceFicheroSh(e){
        this.tipoF = "sh";
        this.generarEnlaceFichero(e);

    }

    generarEnlaceFicheroBat(e){
        this.tipoF = "bat";
        this.generarEnlaceFichero(e);

    }

    generarEnlaceFichero(e){
        console.log("Generar fichero con RepoOrigen " +
        this.idRepoOrigen + ", RepoDestino " + this.idRepoDestino + ", IdPerfil " + this.idPerfil + ", tipo fichero " + this.tipoF);


        let url = "http://localhost:8081/hackgit/v1/archivos/"+this.idPerfil+"/"+this.idRepoOrigen+"/"+this.idRepoDestino+"/"+this.tipoF;
        const a = document.createElement("a");
        a.style.display = "none";
        document.body.appendChild(a);
        a.href = url;
        a.click();
        // limpiamos memoria
        window.URL.revokeObjectURL(a.href);
        document.body.removeChild(a);

    }

    generarEnlaceFicheroMock(){
        console.log("Generar fichero Mock");
    }

}

customElements.define('hg-main-file', HgMainFile);
