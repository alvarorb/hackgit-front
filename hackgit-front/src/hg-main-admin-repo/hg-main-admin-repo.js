import { LitElement, html} from 'lit-element';
import '../hg-lista-repos/hg-lista-repos.js';
import '../hg-form-repo/hg-form-repo.js';

class HgMainAdminRepo extends LitElement {

    static get properties(){
        return {
            idRepoSelected: {type: String}
        }
    }

    constructor(){
        super();

        this.idRepoSelected = "";
    }

    render(){
        return html`
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z"  crossorigin="anonymous" />
            <h2>Administración de repositorios Git</h2>
            <hg-lista-repos @selected-repo-event="${this.selectedRepo}" titulo="Lista de repositorios"></hg-lista-repos>
            <row>
                <a @click="${this.addRepo}" class="btn btn-success"><strong>Añadir repositorio</strong></a>
                <a @click="${this.deleteRepo}" class="btn btn-danger"><strong>Borrar repositorio</strong></a>
            </row>
            <hg-form-repo @form-repo-store-event="${this.formRepoStore}" @form-repo-reset-event="${this.formRepoReset}" class="d-none"></hg-form-repo>
         `;
    }

    addRepo(e){
        console.log("Añadimos un repositorio nuevo");

        this.shadowRoot.querySelector("hg-lista-repos").classList.add("d-none");
        this.shadowRoot.querySelector("hg-form-repo").classList.remove("d-none");
    }

    selectedRepo(e){
        console.log("Repo seleccionado "+e.detail.selectedId);

        this.idRepoSelected=e.detail.selectedId;
    }

    deleteRepo(e){
        console.log("Borramos el repositorio seleccionado");

        let idRepo=this.idRepoSelected;

        let xhr = new XMLHttpRequest();

        xhr.onload = () => {
            console.log("Estado peticion: "+ xhr.status);
            if(xhr.status === 200){
                console.log("Borrado el ID " + idRepo);
                this.shadowRoot.querySelector("hg-lista-repos").refresh = true;
            }
        }

        xhr.open("DELETE", "http://localhost:8081/hackgit/v1/repos/"+idRepo, true);
        xhr.send();

        if(xhr.status != 200){
            console.log("Borrado el ID " + idRepo + " (MOCK)");
        }

    }

    formRepoReset(){
        console.log("formRepoReset");

        this.shadowRoot.querySelector("hg-lista-repos").classList.remove("d-none");
        this.shadowRoot.querySelector("hg-form-repo").classList.add("d-none");
    }

    formRepoStore(){
        console.log("formRepoStore");

        this.shadowRoot.querySelector("hg-lista-repos").classList.remove("d-none");
        this.shadowRoot.querySelector("hg-form-repo").classList.add("d-none");

        this.shadowRoot.querySelector("hg-lista-repos").refresh = true;
    }
}

customElements.define('hg-main-admin-repo', HgMainAdminRepo);
